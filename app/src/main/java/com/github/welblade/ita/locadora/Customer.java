package com.github.welblade.ita.locadora;

import java.util.Vector;

public class Customer {
	
	private String _name;
	private double _totalAmount = 0;
	private int _frequentRenterPoints = 0;
	private Vector<Rental> _rentals = new Vector<Rental>();

	public Customer(String name) {
		_name = name;
	};

	public void addRental(Rental rental) {
		_rentals.addElement(rental);
		_totalAmount += rental.getAmount();
		_frequentRenterPoints += rental.getFrequentRenterPoints();
	}

	public String getName() {
		return _name;
	}

	public String statement() {
		var rentals = _rentals.elements();
		String result = "Rental Record for " + getName() + "\n";
		while (rentals.hasMoreElements()) {
			Rental each = (Rental) rentals.nextElement();
			double thisAmount = each.getAmount();
			//show figures for this rental
			result += "\t" + each.getMovie().getTitle() + "\t"
					+ String.valueOf(thisAmount) + "\n";
		}
		//add footer lines
		result += "Amount owed is " + String.valueOf(getTotalAmount()) + "\n";
		result += "You earned " + String.valueOf(getFrequentRenterPoints())
				+ " frequent renter points";
		return result;
	}

	public double getTotalAmount(){
		return _totalAmount;
	}

	public int getFrequentRenterPoints(){
		return _frequentRenterPoints;
	}
}